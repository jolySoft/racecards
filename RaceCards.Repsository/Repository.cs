﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using NHibernate;
using NHibernate.Linq;
using RaceCards.Domain;

namespace RaceCards.Repsository
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        private readonly ISessionFactory _sessionFactory;
        private ISession _session;

        protected ISession Session
        {
            get { return _session ?? (_session = _sessionFactory.OpenSession()); }
        }

        public Repository(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }

        public virtual T Get(object id)
        {
            var item = Session.Get<T>(id);

            return item;
        }

        public T Load(object id)
        {
            var item = Session.Load<T>(id);

            return item;
        }

        public virtual IList<T> FindAll()
        {

            var items = Session.QueryOver<T>();
            return items.List();
        }

        public T FindSingle(Expression<Func<T, bool>> where)
        {
            var item = Query(where).SingleOrDefault();

            return item;
        }

        public virtual bool Exists(Func<T, bool> where)
        {
            return Session.Linq<T>().Any(where);
        }

        public IEnumerable<T> CreateQuery(string hql)
        {
            var items = Session.CreateQuery(hql).List<T>();

            return items;
        }

        public IQueryOver<T> Query(Expression<Func<T, bool>> @where)
        {
            var items = Session.QueryOver<T>()
                .Where(where);

            return items;
        }

        public virtual void Save(T entity)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                session.SaveOrUpdate(entity);
                session.Flush();
            }
        }

        public void Delete(T entity)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                session.Delete(entity);
            }
        }
    }
}