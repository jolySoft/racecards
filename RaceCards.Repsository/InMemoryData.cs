﻿using RaceCards.Repsository.Mothers;

namespace RaceCards.Repsository
{
    public class InMemoryData : InMemoryDatabase
    {
        public void Create()
        {
            var dogMeeting = DogMeetingMother.CreateDogMeetingListWithEventsAndRunners(0);
            Session.Save(dogMeeting);
            Session.Flush();
        }
    }
}