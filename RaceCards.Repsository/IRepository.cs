﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using NHibernate;
using RaceCards.Domain;

namespace RaceCards.Repsository
{
    public interface IRepository<T> where T : Entity
    {
        T Get(object id);
        T Load(object id);
        IList<T> FindAll();
        T FindSingle(Expression<Func<T, bool>> where);
        IQueryOver<T> Query(Expression<Func<T, bool>> where);
        bool Exists(Func<T, bool> where);
        IEnumerable<T> CreateQuery(string hql); 
        void Save(T entity);
        void Delete(T entity);
    }
}