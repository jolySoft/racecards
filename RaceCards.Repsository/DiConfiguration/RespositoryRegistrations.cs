﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using NHibernate;

namespace RaceCards.Repsository.DiConfiguration
{
    public static class RespositoryRegistrations
    {
        public static void AddTo(IWindsorContainer container)
        {
            var inMemoryDB = new InMemoryData();

            container.Register(Classes.FromThisAssembly().BasedOn(typeof (Repository<>)));
            container.Register(Component.For<ISessionFactory>().Instance(inMemoryDB.CreateSessionFactory()));
        }
    }
}