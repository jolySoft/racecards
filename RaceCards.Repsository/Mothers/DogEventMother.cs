﻿using System;
using System.Collections.Generic;
using System.Linq;
using RaceCards.Common.Enumerables;
using RaceCards.Domain;

namespace RaceCards.Repsository.Mothers

{
    public static class DogEventMother
    {
        private static readonly Random random = new Random();

        private static readonly string[] _raceName = new[] { "THE HANNAH BARBARA CLASSIC", "THE SCOOBY DOO STAKES", "MAIMING MEMORIAL RACE", "DEL BOY BIRTHDAY STAKES", "SIS CLASSIC", "BEYOND THE PALE STAKES", "THE BLACK AND WHITE CLASSIC" };
        private static readonly string[] _raceDistance = new[] { "450mtrs", "630mtrs", "480mtrs" };
        private static readonly string[] _eventGrade = new[] { "A9", "A4", "A3" };
        private static readonly string[] _eventGoing = new[] { "GOOD", "FAIR", "SOFT" };
        private static readonly bool[] _eventHandicap = new[] { true, false };

        private static readonly DogEnumerables.Authority[] _authorities = new[] { DogEnumerables.Authority.BAGS, DogEnumerables.Authority.None, DogEnumerables.Authority.Non__BAGS };
        private static readonly CommonEnumerables.EventStatus[] _eventStatus = new[] { CommonEnumerables.EventStatus.On, CommonEnumerables.EventStatus.Abandoned, CommonEnumerables.EventStatus.Void };
        private static readonly DogEnumerables.EventProgress[] _eventProgress = new[] { DogEnumerables.EventProgress.Parading, DogEnumerables.EventProgress.Going_In_Tps, DogEnumerables.EventProgress.Off, DogEnumerables.EventProgress.Hare_Running, DogEnumerables.EventProgress.Betng_Suspnd };

        public static CommonEnumerables.EventStatus RandomEventStatus
        {
            get { return _eventStatus[random.Next(0, _eventStatus.Count())]; }
        }

        public static DogEnumerables.EventProgress RandomEventProgress
        {
            get { return _eventProgress[random.Next(0, _eventProgress.Count())]; }
        }
        
        public static DogEnumerables.Authority RandomAuthority
        {
            get { return _authorities[random.Next(0, _authorities.Count())]; }
        }

        public static string RandomRaceName
        {
            get { return _raceName[random.Next(0, _raceName.Count())]; }
        }

        public static string RandomRaceDistance
        {
            get { return _raceDistance[random.Next(0, _raceDistance.Count())]; }
        }

        public static string RandomEventGrade
        {
            get { return _eventGrade[random.Next(0, _eventGrade.Count())]; }
        }

        public static string RandomEventGoing
        {
            get { return _eventGoing[random.Next(0, _eventGoing.Count())]; }
        }

        public static bool RandomRaceHandicap
        {
            get { return _eventHandicap[random.Next(0, _eventHandicap.Count())]; }
        }



        public static DogEvent CreateDogEvent(int id, DateTime startTime, int meetingId)
        {
            return CreateDogEvent(id, startTime, RandomRaceName, RandomEventStatus, RandomEventProgress, RandomRaceHandicap, meetingId);
        }

        public static DogEvent CreateDogEvent(int id)
        {
            return CreateDogEvent(id, DateTime.Now.AddMinutes(2), RandomRaceName, CommonEnumerables.EventStatus.On, RandomEventProgress, RandomRaceHandicap, 999999);
        }

        public static DogEvent CreateDogEventWithNoResult(int id)
        {
            return CreateDogEvent(id, DateTime.Now.AddMinutes(2), RandomRaceName, CommonEnumerables.EventStatus.On, RandomEventProgress, RandomRaceHandicap, 999999, false);
        }

        public static DogEvent CreateOffedDogEvent(int id)
        {
            return CreateDogEvent(id, DateTime.Now.AddMinutes(2), RandomRaceName, CommonEnumerables.EventStatus.On, DogEnumerables.EventProgress.Off, RandomRaceHandicap, 999999);
        }

        public static DogEvent CreateDogEventWithRunners(int id, int numberOfRunners = 6)
        {
            var dogEvent = CreateDogEvent(id);
            dogEvent.Runners = DogMother.CreateDogs(numberOfRunners, null).ToList(); // creates dogs with prices (opening show)
            return dogEvent;
        }

        public static DogEvent CreateOffedDogEventWithRunners(int id, int numberOfRunners = 6)
        {
            var dogEvent = CreateOffedDogEvent(id);
            dogEvent.Runners = DogMother.CreateDogs(numberOfRunners, null).ToList(); // creates dogs with prices (opening show)
            return dogEvent;
        }

        public static IEnumerable<DogEvent> CreateDogEvents(int meetingId, int numberOfEvents)
        {
            var random = new Random(DateTime.Now.Millisecond);

            for (int i = 1; i <= numberOfEvents; i++)
            {
                yield return CreateDogEvent(i, DateTime.Now.AddMilliseconds(random.Next(1000, 100000)), meetingId);
            }
            yield break;
        }

        public static List<DogEvent> CreateDogEvents(int numberOfEvents)
        {
            var random = new Random(DateTime.Now.Millisecond);

            var dogList = new List<DogEvent>();

            for (int i = 1; i <= numberOfEvents; i++)
            {
                dogList.Add(CreateDogEvent(i, DateTime.Now.AddMilliseconds(random.Next(1000, 100000)), 1));
            }
            return dogList;
        }

        public static DogEvent CreateDogEvent(int id, DateTime startTime, string name, CommonEnumerables.EventStatus eventStatus, DogEnumerables.EventProgress eventProgress, bool eventHandicap, int meetingId, bool withResult = true)
        {
            var dogEvent = new DogEvent
                               {
                                   Id = id,
                                   Number = id,
                                   Name = name,
                                   StartTime = startTime,
                                   Status = eventStatus,
                                   Progress = eventProgress,
                                   Handicap = eventHandicap,
                                   Distance = RandomRaceDistance, 
                                   EventGrade = RandomEventGrade, 
                                   Going = RandomEventGoing,
                                   PredictedRunnerCount = 10,
                                   ClassificationCode = "OW",
                                   SurfaceType = DogEnumerables.SurfaceType.Dirt,
                                   Meeting = new DogMeeting() { Id = meetingId, Status = CommonEnumerables.MeetingStatus.On},
                               };
            dogEvent.Runners = DogMother.CreateDogs(6, dogEvent).ToList();
            return dogEvent;
        }
    }
}
