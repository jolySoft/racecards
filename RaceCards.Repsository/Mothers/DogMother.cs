﻿using System;
using System.Collections.Generic;
using System.Linq;
using RaceCards.Common.Enumerables;
using RaceCards.Domain;

namespace RaceCards.Repsository.Mothers
{
    public static class DogMother
    {
        private static readonly Random random = new Random();

        private static readonly string[] _dogName = new[]
        {
            "Cunning Stunt", "Where is Waldo", "Coming up", "Trailer Park",
            "Big Sis", "Pennys Missing", "Rooskey Rascal"
        };

        public static string RandomDogName
        {
            get { return _dogName[random.Next(0, _dogName.Count())]; }
        }


        public static Dog CreateDog(int id)
        {
            var dog = new Dog
            {
                Id = id,
                Name = RandomDogName + (id + 1),
                RunnerNumber = (id + 1).ToString(),
                Status = DogEnumerables.DogStatus.Goes_as_expected,
                Source = "SIS",
                SelectionNumber = (id + 1),
                Coupling = "N",
                DrawNumber = (id + 1).ToString(),
                LockFlag = "N",
            };

            return dog;
        }

        public static Dog CreateDog(int id, DogEvent dogEvent)
        {
            var dog = CreateDog(id);
            dog.Event = dogEvent;

            return dog;
        }

        public static IEnumerable<Dog> CreateDogs(int numberOfDogs, DogEvent dogEvent)
        {
            for (int i = 1; i <= numberOfDogs; i++)
            {
                yield return CreateDog(i, dogEvent);
            }

            yield break;
        }
    }
}