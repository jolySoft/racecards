﻿using System;
using System.Collections.Generic;
using System.Linq;
using RaceCards.Common.Enumerables;
using RaceCards.Domain;

namespace RaceCards.Repsository.Mothers
{
    public static class DogMeetingMother
    {
        private static readonly Random random = new Random();
        private static readonly DateTime? dateTimeNull = null;

        private static readonly DogEnumerables.Authority[] _authorities = new[] { DogEnumerables.Authority.BAGS, DogEnumerables.Authority.None, DogEnumerables.Authority.Non__BAGS };
        private static readonly CommonEnumerables.DataProvider[] _dataProviders = new[] { CommonEnumerables.DataProvider.Press_Association, CommonEnumerables.DataProvider.SIS, CommonEnumerables.DataProvider.Formnet, CommonEnumerables.DataProvider.SISFill };
        private static readonly DogEnumerables.ServiceType[] _serviceType = new[] { DogEnumerables.ServiceType.Full, DogEnumerables.ServiceType.Results_Only };
        private static readonly DogEnumerables.Service[] _service = new[] { DogEnumerables.Service.Main, DogEnumerables.Service.Evening_Dogs, DogEnumerables.Service.Early_Morning_Product };
        private static readonly DogEnumerables.SurfaceType[] _surface = new[] { DogEnumerables.SurfaceType.Dirt, DogEnumerables.SurfaceType.Outdoor, DogEnumerables.SurfaceType.Unknown };
        private static readonly CommonEnumerables.MeetingStatus[] _meetingStatus = new[] { CommonEnumerables.MeetingStatus.On, CommonEnumerables.MeetingStatus.Abandoned, CommonEnumerables.MeetingStatus.Void, CommonEnumerables.MeetingStatus.Postponed, CommonEnumerables.MeetingStatus.Postponed };
        private static readonly CommonEnumerables.Country[] _countries = new[] { CommonEnumerables.Country.United_Kingdom };

        private static readonly string[] _going = new[] { "NORMAL", "SLOW" };
        private static readonly string[] _sportCode = new[] { "PD", "YD", "HA", "KS", "CD", "MK", "ND" };
        private static readonly string[] _trackName = new[] { "Poole", "Yarmouth", "Harlow", "Kinsley", "Cheltenham", "Milton Keynes", "Nottingham" };
        private static readonly string[] _rawName = new[] { "Poole", "Yarmouth", "Harlow", "Kinsley", "Chelt' Dogs", "Milton Key's", "Notts (Dogs)" };
        private static readonly DateTime?[] _publicationStamp = new[] { dateTimeNull, DateTime.Now.AddMinutes(random.Next(-100, 0)) };
        private static readonly bool[] _promoteFlag = new[] { true, false };

        public static string RandomSportCode
        {
            get { return _sportCode[random.Next(0, _sportCode.Count())]; }
        }

        public static string RandomTrackName
        {
            get { return _trackName[random.Next(0, _trackName.Count())]; }
        }

        public static string RandomRawName
        {
            get { return _rawName[random.Next(0, _rawName.Count())]; }
        }

        public static string RandomGoing
        {
            get { return _going[random.Next(0, _going.Count())]; }
        }

        public static CommonEnumerables.Country RandomCountry
        {
            get { return _countries[random.Next(0, _countries.Count())]; }
        }

        public static bool RandomPromoteFlag
        {
            get { return _promoteFlag[random.Next(0, _promoteFlag.Count())]; }
        }

        public static DateTime? RandomPublicationStamp
        {
            get { return _publicationStamp[random.Next(0, _publicationStamp.Count())]; }
        }

        public static DogEnumerables.Authority RandomAuthority
        {
            get { return _authorities[random.Next(0, _authorities.Count())]; }
        }

        public static CommonEnumerables.DataProvider RandomDataProvider
        {
            get { return _dataProviders[random.Next(0, _dataProviders.Count())]; }
        }

        public static DogEnumerables.ServiceType RandomServiceType
        {
            get { return _serviceType[random.Next(0, _serviceType.Count())]; }
        }

        public static DogEnumerables.Service RandomService
        {
            get { return _service[random.Next(0, _service.Count())]; }
        }

        public static DogEnumerables.SurfaceType RandomSurfaceType
        {
            get { return _surface[random.Next(0, _surface.Count())]; }
        }

        public static CommonEnumerables.MeetingStatus RandomMeetingStatus
        {
            get { return _meetingStatus[random.Next(0, _meetingStatus.Count())]; }
        }

        public static DogMeeting CreateNewDogMeeting()
        {
            return CreateDogMeeting(0);
        }

        public static DogMeeting CreateDogMeeting(int id)
        {
            var dogMeeting = CreateDogMeeting(id,
                                        RandomSportCode,
                                        RandomTrackName,
                                        DateTime.Now,
                                        RandomPublicationStamp,
                                        RandomPromoteFlag,
                                        RandomGoing,
                                        RandomCountry,
                                        RandomAuthority,
                                        RandomDataProvider,
                                        RandomServiceType,
                                        RandomService,
                                        RandomSurfaceType,
                                        RandomMeetingStatus, 10);
            return dogMeeting;
        }

        public static DogMeeting CreateDogMeetingWithoutEvents(int id)
        {
            var dogMeeting = CreateDogMeeting(id,
                                        RandomSportCode,
                                        RandomTrackName,
                                        DateTime.Now,
                                        RandomPublicationStamp,
                                        RandomPromoteFlag,
                                        RandomGoing,
                                        RandomCountry,
                                        RandomAuthority,
                                        RandomDataProvider,
                                        RandomServiceType,
                                        RandomService,
                                        RandomSurfaceType,
                                        RandomMeetingStatus, 0);
            return dogMeeting;
        }

        public static DogMeeting CreateDogMeeting(int id, DogEnumerables.Authority authority)
        {
            return CreateDogMeeting(id,
                                        RandomSportCode,
                                        RandomTrackName,
                                        DateTime.Now,
                                        RandomPublicationStamp,
                                        RandomPromoteFlag,
                                        RandomGoing,
                                        RandomCountry,
                                        authority,
                                        RandomDataProvider,
                                        RandomServiceType,
                                        RandomService,
                                        RandomSurfaceType,
                                        RandomMeetingStatus, 10);
        }

        public static DogMeeting CreateDogMeeting(int id, int numberOfEvents, DogEnumerables.Authority authority, CommonEnumerables.MeetingStatus meetingStatus, DateTime startTime)
        {
            return CreateDogMeeting(id,
                                        RandomSportCode,
                                        RandomTrackName,
                                        startTime,
                                        RandomPublicationStamp,
                                        RandomPromoteFlag,
                                        RandomGoing,
                                        RandomCountry,
                                        authority,
                                        RandomDataProvider,
                                        RandomServiceType,
                                        RandomService,
                                        RandomSurfaceType,
                                        meetingStatus,
                                        numberOfEvents);
        }

        public static DogMeeting CreateDogMeeting(int id, string sportCode, string trackName, DateTime startDate, DateTime? publicationStamp,
            bool promoteFlag, string going, CommonEnumerables.Country country, DogEnumerables.Authority authority, CommonEnumerables.DataProvider dataProvider,
            DogEnumerables.ServiceType serviceType, DogEnumerables.Service service, DogEnumerables.SurfaceType surfaceType,
            CommonEnumerables.MeetingStatus meetingStatus, int numberOfEvents)
        {
            var dogMeeting = new DogMeeting
                                 {
                                     Id = id,
                                     SportCode = sportCode,
                                     Status = meetingStatus,
                                     StartDate = startDate.Date,
                                     Authority = authority,
                                     Name = trackName,
                                     Country = country,
                                     Going = going,
                                     ServiceType = serviceType,
                                     Service = service,
                                     SurfaceType = surfaceType,
                                     PredictedEventCount = numberOfEvents,
                                 };

            IEnumerable<DogEvent> dogEvent = DogEventMother.CreateDogEvents(dogMeeting.Id, numberOfEvents).OrderBy(c => c.StartTime).ToList();

            //since meeting events are already sequenced when returned from db
            dogMeeting.Events = ResequenceMeetingEvents(dogEvent.ToList());

            return dogMeeting;
        }

        private static IList<DogEvent> ResequenceMeetingEvents(IList<DogEvent> events)
        {
            for (var i = 0; i < events.Count(); i++)
            {
                events.OrderBy(x => x.StartTime).ElementAt(i).Number = i + 1; //note: 0 based, event numbers start with 1
            }

            return events;
        }

        public static IList<DogMeeting> CreateDogMeetings()
        {
            return CreateDogMeetings(RandomPublicationStamp, RandomMeetingStatus, RandomPromoteFlag).ToList();
        }

        public static IEnumerable<DogMeeting> CreateDogMeetings(DateTime? publicationStamp, CommonEnumerables.MeetingStatus meetingStatus, bool promoteFlag)
        {
            for (int i = 1; i <= 12; i++)
            {
                var dogMeeting = CreateDogMeeting(i);

                dogMeeting.PredictedEventCount = dogMeeting.Events.Count;
                dogMeeting.StartDate = dogMeeting.Events.OrderBy(c => c.StartTime).Select(c => c.StartTime).FirstOrDefault();

                yield return dogMeeting;
            }

            yield break;
        }

        public static IEnumerable<DogMeeting> CreateDogMeetingListWithEventsAndRunners(int meetingId)
        {
                var dogMeeting = CreateDogMeeting(meetingId);

                dogMeeting.PredictedEventCount = dogMeeting.Events.Count;

                foreach (var dogEvent in dogMeeting.Events)
                {
                    dogEvent.Runners = DogMother.CreateDogs(6, dogEvent).ToList();
                }

                dogMeeting.StartDate = dogMeeting.Events.OrderBy(c => c.StartTime).Select(c => c.StartTime).FirstOrDefault();

                yield return dogMeeting;

            yield break;
        }
    }
}
