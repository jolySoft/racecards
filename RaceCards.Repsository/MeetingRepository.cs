﻿using System.Linq;
using NHibernate;
using NHibernate.Linq;
using RaceCards.Domain;

namespace RaceCards.Repsository
{
    public class MeetingRepository : Repository<DogMeeting>
    {
        public MeetingRepository(ISessionFactory sessionFactory) : base(sessionFactory) { }

        public IQueryable<Meeting> GetFullCard()
        {
            return Session.QueryOver<DogMeeting>()
                .Fetch(m => m.Events)
                .Eager.Future().AsQueryable();
        }

        public IQueryable<Meeting> GetCard()
        {
            return Session.QueryOver<DogMeeting>().As<IQueryable<Meeting>>();
        }
    }
}