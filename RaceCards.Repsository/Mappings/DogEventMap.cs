﻿using FluentNHibernate.Mapping;
using RaceCards.Common.Enumerables;
using RaceCards.Domain;

namespace RaceCards.Repsository.Mappings
{
    public class DogEventMap : ClassMap<DogEvent>
    {
        public DogEventMap()
        {
            Table("EVENT");

            Id(x => x.Id).UniqueKey("ID").GeneratedBy.Sequence("SEQ_EVENT");

            this.SharedEventMappings();
            References(x => x.Meeting).Column("MEETINGID").Class(typeof(DogMeeting)).Not.LazyLoad();
            Map(x => x.Progress).Column("ProgressCode").CustomType<DogEventProgressEnum>();
            Map(x => x.Status).CustomType<EventStatusEnum>();
            Map(x => x.SurfaceType).Column("SurfaceTypeCode").CustomType<DogSurfaceTypeEnum>();          
            Map(x => x.CourseType).Column("COURSETYPECODE").CustomType<DogCourseTypeEnum>();
            HasMany(x => x.Runners).KeyColumn("EVENTID").LazyLoad().Cascade.AllDeleteOrphan().Inverse().OrderBy("RUNNERNUMBER asc").Fetch.Subselect();
        }
    }
}