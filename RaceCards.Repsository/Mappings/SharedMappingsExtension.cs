using FluentNHibernate.Mapping;
using NHibernate.Type;
using RaceCards.Common.Enumerables;
using RaceCards.Domain;

namespace RaceCards.Repsository.Mappings
{
    public static class SharedMappingsExtension
    {
        public static void SharedEventMappings<T>(this ClassMap<T> mapper) where T : Event
        {
            mapper.Map(x => x.Comments);
            mapper.Map(x => x.EventGrade);
            mapper.Map(x => x.Number).Column("EventNumber");
            mapper.Map(x => x.Handicap).CustomType<YesNoType>();
            mapper.Map(x => x.Name).Column("DESCRIPTION");
            mapper.Map(x => x.EachwayPlaces);
            mapper.Map(x => x.ResultPlacesExpected).Column("ResultPlacesExpected");
            mapper.Map(x => x.PredictedRunnerCount).Column("SELECTIONS");
            mapper.Map(x => x.StartTime).Column("STARTDATE");

        } 

        public static void SharedMeetingMappings<T>(this ClassMap<T> mapper) where T : Meeting
        {
            mapper.Map(x => x.CategoryCode).Column("CategoryCode").CustomType<CategoryCodeEnum>();
            mapper.Map(x => x.Comments);
            mapper.Map(x => x.Country).Column("COUNTRYCODE").CustomType<CountryEnum>();
            mapper.Map(x => x.DailyCode);
            mapper.Map(x => x.DurationDays);
            mapper.Map(x => x.Location);
            mapper.Map(x => x.MeetingCode).Length(5);
            mapper.Map(x => x.Name).Column("DESCRIPTION").Not.Nullable();
            mapper.Map(x => x.PredictedEventCount).Column("NumberOfEvents");
            mapper.Map(x => x.SportCode);
            mapper.Map(x => x.StartDate).Not.Nullable();

            mapper.Join("MEETINGGOING", c =>
            {
                c.Fetch.Join();
                c.KeyColumn("MEETINGID");
                c.Optional();
                c.Map(x => x.Going, "FREEFORMGOING");
            });
        } 

        public static void SharedSelectionMappings<T>(this ClassMap<T> mapper) where T : Runner
        {
            mapper.Map(x => x.Comments).Length(1000);
            mapper.Map(x => x.CountryOfOrigin).Length(2); //note: enum/dropdown list etc.
            mapper.Map(x => x.Coupling).Length(1); //note: enum
            mapper.Map(x => x.DrawNumber).Length(3);
            mapper.Map(x => x.Gender).Length(1); //note: enum
            mapper.Map(x => x.LockFlag).Column("LOCK_FLAG").Length(1); //note: enum
            mapper.Map(x => x.Name).Length(30);
            mapper.Map(x => x.ReserveName).Length(30);
            mapper.Map(x => x.Source);
            mapper.Map(x => x.TimeOfIssue).Nullable();
            mapper.Map(x => x.UniqueId).Length(12);
        }
    }
}
