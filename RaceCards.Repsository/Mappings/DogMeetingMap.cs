﻿using FluentNHibernate.Mapping;
using RaceCards.Common.Enumerables;
using RaceCards.Domain;

namespace RaceCards.Repsository.Mappings
{
    public class DogMeetingMap : ClassMap<DogMeeting>
    {
        public DogMeetingMap()
        {
            Table("MEETING");

            Id(x => x.Id).UniqueKey("ID").GeneratedBy.Sequence("SEQ_MEETING");
            Where("CategoryCode = 'DG'");

            this.SharedMeetingMappings();
            
            Map(x => x.Authority).Column("SPORTSUBCODE").CustomType<DogMeetingAuthorityEnum>();
            Map(x => x.Status).CustomType<MeetingStatusEnum>();
            Map(x => x.SurfaceType).Column("SurfaceTypeCode").CustomType<DogSurfaceTypeEnum>();
            Map(x => x.Service).CustomType<DogEventServiceEnum>();
            Map(x => x.ServiceType).CustomType<DogMeetingServiceTypeEnum>();
            
            HasMany(x => x.Events).KeyColumn("MEETINGID").Cascade.AllDeleteOrphan().Inverse().Not.LazyLoad().OrderBy("EVENTNUMBER asc").Fetch.Subselect();
        }
    }
}
