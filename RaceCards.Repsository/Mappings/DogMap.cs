﻿using FluentNHibernate.Mapping;
using RaceCards.Common.Enumerables;
using RaceCards.Domain;

namespace RaceCards.Repsository.Mappings
{
    public class DogMap : ClassMap<Dog>
    {
        public DogMap()
        {
            Table("SELECTION");
            
            Id(x => x.Id).UniqueKey("ID").GeneratedBy.Sequence("SEQ_SELECTION");

            this.SharedSelectionMappings();

            Map(x => x.RunnerNumber).Length(3);
            References(x => x.Event).Column("EVENTID").LazyLoad().Class(typeof(DogEvent));
            Map(x => x.Status).CustomType<DogStatusEnum>();
            Map(x => x.SelectionNumber);
        }
    }
}