﻿using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace BetClearer.UI.DiConfiguration
{
    public static class FacadeRegistrations
    {
         public static void AddTo(IWindsorContainer container)
         {
             container.Register(Classes.FromThisAssembly().BasedOn<IController>().LifestylePerWebRequest(),
                                Classes.FromThisAssembly().BasedOn<IHttpController>().LifestylePerWebRequest(),                                
                                Component.For<IHttpControllerActivator>().ImplementedBy<WindsorHttpControllerActivator>());
             
             ControllerBuilder.Current.SetControllerFactory(new WindsorControllerFactory(container));
             GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new WindsorHttpControllerActivator(container));             
         }
    }
}