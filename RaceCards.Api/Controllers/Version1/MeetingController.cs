﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using RaceCards.Api.Dto;
using RaceCards.Infrastructure;

namespace RaceCards.Api.Controllers.Version1
{
    public class MeetingController : ApiController
    {
        private readonly MeetingService _meetingService;

        public MeetingController(MeetingService meetingService)
        {
            _meetingService = meetingService;
        }

        [HttpGet]
        public FullRaceRacd Get()
        {
            var meeting = _meetingService.GetCard(true);
            return Mapper.Map<FullRaceRacd>(meeting);
        }
    }
}
