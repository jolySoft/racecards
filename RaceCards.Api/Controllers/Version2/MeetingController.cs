﻿using System.Web.Http;
using AutoMapper;
using RaceCards.Api.Dto;
using RaceCards.Infrastructure;

namespace RaceCards.Api.Controllers.Version2
{
    public class MeetingController
    {
        private readonly MeetingService _meetingService;

        public MeetingController(MeetingService meetingService)
        {
            _meetingService = meetingService;
        }

        [HttpGet]
        public PartialRaceCard Get()
        {
            var meeting = _meetingService.GetCard(true);
            return Mapper.Map<PartialRaceCard>(meeting);
        }         
    }
}