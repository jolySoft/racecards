﻿using System.Configuration;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Routing;
using BetClearer.UI.DiConfiguration;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using RaceCards.Api.DiConfiguration;
using RaceCards.Api.Dto;
using RaceCards.Infrastructure.DiConfiguration;
using RaceCards.Repsository.DiConfiguration;
using SDammann.WebApi.Versioning;

namespace RaceCards.Api
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        private static IWindsorContainer _container;

        public IWindsorContainer Container
        {
            get { return _container; }
        }


        protected void Application_Start()
        {

            _container = new WindsorContainer();
            RegisterIoC();
            CreateMaps();

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerSelector),
                                               new VersionHeaderVersionedControllerSelector(GlobalConfiguration.Configuration));

            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new WindsorHttpControllerActivator(_container)); 
        }

        private static void RegisterIoC()
        {
            _container.Register(Component.For<IWindsorContainer>().Instance(_container));
            ServiceRegistration.AddTo(_container);
            RespositoryRegistrations.AddTo(_container);
            ApiRegistrations.AddTo(_container);
        }

        private static void CreateMaps()
        {
            DtoMappings.CreateMaps();
        }
    }
}