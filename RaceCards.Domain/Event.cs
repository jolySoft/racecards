﻿using System;
using RaceCards.Common.Enumerables;

namespace RaceCards.Domain
{
    public abstract class Event : Entity, IEquatable<Event>
    {
        public virtual string ClassificationCode { get; set; }
        public virtual string Comments { get; set; }
        public virtual string CourseType { get; set; }
        public virtual string Distance { get; set; }
        public virtual string EventGrade { get; set; }
        public virtual int Number { get; set; }
        public virtual String Going { get; set; }
        public virtual bool Handicap { get; set; }
        public virtual bool LockFlag { get; set; }
        public virtual string Name { get; set; }
        public virtual int NewShow { get; set; }
        public virtual DateTime StartTime { get; set; }
        public virtual CommonEnumerables.EventStatus Status { get; set; }
        public virtual int PredictedRunnerCount { get; set; }
        public virtual int ResultPlacesExpected { get; set; }
        public virtual int EachwayPlaces { get; set; }
        public virtual string EachwayTerms { get; set; }

        public bool Equals(Event other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && string.Equals(ClassificationCode, other.ClassificationCode) &&
                   string.Equals(Comments, other.Comments) && string.Equals(CourseType, other.CourseType) &&
                   string.Equals(Distance, other.Distance) && string.Equals(EventGrade, other.EventGrade) &&
                   Number == other.Number && string.Equals(Going, other.Going) && Handicap.Equals(other.Handicap) &&
                   LockFlag.Equals(other.LockFlag) && string.Equals(Name, other.Name) && NewShow == other.NewShow &&
                   StartTime.Equals(other.StartTime) && Status == other.Status &&
                   PredictedRunnerCount == other.PredictedRunnerCount &&
                   ResultPlacesExpected == other.ResultPlacesExpected && EachwayPlaces == other.EachwayPlaces &&
                   string.Equals(EachwayTerms, other.EachwayTerms);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Event) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode*397) ^ (ClassificationCode != null ? ClassificationCode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Comments != null ? Comments.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (CourseType != null ? CourseType.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Distance != null ? Distance.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (EventGrade != null ? EventGrade.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ Number;
                hashCode = (hashCode*397) ^ (Going != null ? Going.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ Handicap.GetHashCode();
                hashCode = (hashCode*397) ^ LockFlag.GetHashCode();
                hashCode = (hashCode*397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ NewShow;
                hashCode = (hashCode*397) ^ StartTime.GetHashCode();
                hashCode = (hashCode*397) ^ (int) Status;
                hashCode = (hashCode*397) ^ PredictedRunnerCount;
                hashCode = (hashCode*397) ^ ResultPlacesExpected;
                hashCode = (hashCode*397) ^ EachwayPlaces;
                hashCode = (hashCode*397) ^ (EachwayTerms != null ? EachwayTerms.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Event left, Event right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Event left, Event right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            return "Event: \nID: {0} \nName: {1} \nStartTime: {2} \nNumber: {3} \nPredictedRunnerCount: {4}".Fill(Id,
                Name,
                StartTime,
                Number,
                PredictedRunnerCount);
        }
    }
}