﻿using System;
using System.Collections.Generic;
using RaceCards.Common.Enumerables;

namespace RaceCards.Domain
{
    public class DogEvent : Event, IEquatable<DogEvent>
    {
        public virtual IList<Dog> Runners { get; set; }
        public virtual DogMeeting Meeting { get; set; }
        public virtual DogEnumerables.SurfaceType SurfaceType { get; set; }
        public new virtual DogEnumerables.EventProgress Progress { get; set; }
        public new virtual DogEnumerables.CourseType CourseType{ get; set; }      

        public DogEvent()
        {
            Runners = new List<Dog>();
        }

        public override string ToString()
        {
            return "Dog" + base.ToString();
        }

        public virtual bool Equals(DogEvent other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && Equals(other.Runners, Runners) && Equals(other.Meeting, Meeting) && Equals(other.Status, Status) && Equals(other.SurfaceType, SurfaceType) && Equals(other.Progress, Progress) && Equals(other.CourseType, CourseType) && other.Handicap.Equals(Handicap) && other.LockFlag.Equals(LockFlag);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as DogEvent);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = base.GetHashCode();
                result = (result * 397) ^ (Runners != null ? Runners.GetHashCode() : 0);
                result = (result * 397) ^ (Meeting != null ? Meeting.GetHashCode() : 0);
                result = (result * 397) ^ SurfaceType.GetHashCode();
                result = (result * 397) ^ Progress.GetHashCode();
                result = (result * 397) ^ CourseType.GetHashCode();
                result = (result * 397) ^ Handicap.GetHashCode();
                result = (result * 397) ^ LockFlag.GetHashCode();
                return result;
            }
        }

        public static bool operator ==(DogEvent left, DogEvent right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DogEvent left, DogEvent right)
        {
            return !Equals(left, right);
        }
    }
}

