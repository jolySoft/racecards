﻿using System;
using System.Collections.Generic;
using RaceCards.Common.Enumerables;

namespace RaceCards.Domain
{
    public class DogMeeting : Meeting, IEquatable<DogMeeting>
    {
        public virtual new IList<DogEvent> Events { get; set; }
        public virtual DogEnumerables.SurfaceType SurfaceType { get; set; }
        public virtual DogEnumerables.Authority Authority { get; set; }
        public virtual DogEnumerables.ServiceType ServiceType { get; set; }
        public virtual DogEnumerables.Service Service { get; set; }


        public DogMeeting()
        {
            CategoryCode = CommonEnumerables.CategoryCode.Dog;
            Events = new List<DogEvent>();
        } 

        public override string ToString()
        {
            return "Dog{0} \n Authority: {1} \n DataProvider: {2} \n Status: {3} \n Service: {4} \n ServiceType: {5}".Fill(base.ToString(),
                                                                                    Authority,
                                                                                    Status,
                                                                                    Service,
                                                                                    ServiceType);
        }

        public virtual bool Equals(DogMeeting other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && Equals(other.Events, Events) && Equals(other.SurfaceType, SurfaceType) && Equals(other.Authority, Authority) && Equals(other.ServiceType, ServiceType) && Equals(other.Service, Service);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as DogMeeting);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = base.GetHashCode();
                result = (result*397) ^ (Events != null ? Events.GetHashCode() : 0);
                result = (result*397) ^ SurfaceType.GetHashCode();
                result = (result*397) ^ Authority.GetHashCode();
                result = (result*397) ^ ServiceType.GetHashCode();
                result = (result*397) ^ Service.GetHashCode();
                return result;
            }
        }

        public static bool operator ==(DogMeeting left, DogMeeting right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DogMeeting left, DogMeeting right)
        {
            return !Equals(left, right);
        }
    }
}
