﻿using System;
using RaceCards.Common.Enumerables;

namespace RaceCards.Domain
{
    public abstract class Meeting : Entity, IEquatable<Meeting>
    {
        public virtual string MeetingCode { get; set; }

        public virtual int Priority { get; set; }

        public virtual DateTime StartDate { get; set; }

        public virtual CommonEnumerables.MeetingStatus Status { get; set; }

        public virtual string Name { get; set; } //aka: description

        public virtual string NeutralGround { get; set; }

        public virtual int DurationDays { get; set; }


        public virtual string SurfaceTypeCode { get; set; }

        public virtual string Comments { get; set; }

        public virtual string DailyCode { get; set; }

        public virtual int PredictedEventCount { get; set; }

        public virtual int NumberOfTeams { get; set; }

        public virtual string Location { get; set; }


        public virtual string Progress { get; set; }
        public virtual string Weather { get; set; }

        public virtual CommonEnumerables.CategoryCode CategoryCode { get; set; }

        public virtual string SportCode { get; set; }

        public virtual CommonEnumerables.Country Country { get; set; }

        public virtual String Going { get; set; }

        public bool Equals(Meeting other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && string.Equals(MeetingCode, other.MeetingCode) && Priority == other.Priority &&
                   StartDate.Equals(other.StartDate) && Status == other.Status && string.Equals(Name, other.Name) &&
                   string.Equals(NeutralGround, other.NeutralGround) && DurationDays == other.DurationDays &&
                   string.Equals(SurfaceTypeCode, other.SurfaceTypeCode) && string.Equals(Comments, other.Comments) &&
                   string.Equals(DailyCode, other.DailyCode) && PredictedEventCount == other.PredictedEventCount &&
                   NumberOfTeams == other.NumberOfTeams && string.Equals(Location, other.Location) &&
                   string.Equals(Progress, other.Progress) && string.Equals(Weather, other.Weather) &&
                   CategoryCode == other.CategoryCode && string.Equals(SportCode, other.SportCode) &&
                   Country == other.Country && string.Equals(Going, other.Going);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Meeting) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = base.GetHashCode();
                hashCode = (hashCode*397) ^ (MeetingCode != null ? MeetingCode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ Priority;
                hashCode = (hashCode*397) ^ StartDate.GetHashCode();
                hashCode = (hashCode*397) ^ (int) Status;
                hashCode = (hashCode*397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (NeutralGround != null ? NeutralGround.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ DurationDays;
                hashCode = (hashCode*397) ^ (SurfaceTypeCode != null ? SurfaceTypeCode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Comments != null ? Comments.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (DailyCode != null ? DailyCode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ PredictedEventCount;
                hashCode = (hashCode*397) ^ NumberOfTeams;
                hashCode = (hashCode*397) ^ (Location != null ? Location.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Progress != null ? Progress.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Weather != null ? Weather.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (int) CategoryCode;
                hashCode = (hashCode*397) ^ (SportCode != null ? SportCode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (int) Country;
                hashCode = (hashCode*397) ^ (Going != null ? Going.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Meeting left, Meeting right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Meeting left, Meeting right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            return
                "Meeting: \n ID: {0} \n Name: {1} \n StartDate: {2} \n PredictedEventCount: {3}\n SportCode: {4}\n Going: {5}"
                    .Fill(Id,
                        Name,
                        StartDate,
                        PredictedEventCount,
                        SportCode,
                        Going);
        }
    }
}