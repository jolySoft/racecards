﻿using System;

namespace RaceCards.Domain
{
    public abstract class Entity : IEquatable<Entity>
    {
        public static int Transient = 0;

        private int _id;

        protected Entity() { }

        protected Entity(int id)
        {
            this._id = id;
        }

        public virtual int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public virtual bool IsTransient
        {
            get { return _id == Transient; }
        }

        public virtual bool Equals(Entity other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other._id == _id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (Entity)) return false;
            return Equals((Entity) obj);
        }

        public static bool operator ==(Entity left, Entity right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Entity left, Entity right)
        {
            return !Equals(left, right);
        }

        public override int GetHashCode()
        {
            return _id;
        }
    }
}
