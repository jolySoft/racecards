﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RaceCards.Domain
{
    public class Runner : Entity
    {
        private string _name;
        private string _reserveName;

        public virtual int SelectionNumber { get; set; }

        [StringLength(30), Required(ErrorMessage = "Name is required.")]
        public virtual string Name { get; set; }
     
        [StringLength(3)]
        public virtual string RunnerNumber { get; set; }

        [StringLength(3)]
        public virtual string DrawNumber { get; set; }

        [StringLength(1)]
        public virtual string Gender { get; set; } //note: enum?

        [StringLength(2)]
        public virtual string CountryOfOrigin { get; set; }//note: enum?
       
        [StringLength(1000)]
        public virtual string Comments { get; set; }

        [StringLength(12)]
        public virtual string UniqueId { get; set; }
        
        [Required(ErrorMessage = "DataProvider is required.")]
        public virtual string Source { get; set; }

        [StringLength(30)]
        public virtual string ReserveName { get; set; }

        [DataType(DataType.DateTime)]
        public virtual DateTime? TimeOfIssue { get; set; }

        [StringLength(1)]
        public virtual string Coupling { get; set; }//note: enum?

        [StringLength(1), Required(ErrorMessage = "LockFlag is required.")]
        public virtual string LockFlag { get; set; }//note: enum?


        public override string ToString()
        {
            return "Runner: \n ID: {0} \nName: {1} \n RunnerNumber: {2} \n Status: {3} \n SelectionNumber: {4} \n ResultPlace: {4}".Fill(Id,
                                                                                                                  Name,
                                                                                                                  RunnerNumber,
                                                                                                                  SelectionNumber);
        }
    }

}
