﻿using RaceCards.Common.Enumerables;

namespace RaceCards.Domain
{
    public class Dog : Runner
    {
        public virtual DogEnumerables.DogStatus Status { get; set; }
        public virtual DogEvent Event { get; set; }

        public Dog() { }

        public override string ToString()
        {
            return "Dog" + base.ToString();
        }
    }
}
