﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace RaceCards.Infrastructure.DiConfiguration
{
    public static class ServiceRegistration
    {
        public static void AddTo(IWindsorContainer container)
        {
            container.Register(Component.For<MeetingService>());
        }
    }
}