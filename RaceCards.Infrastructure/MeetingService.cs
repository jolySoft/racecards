﻿using System.Collections.Generic;
using System.Linq;
using RaceCards.Domain;
using RaceCards.Repsository;

namespace RaceCards.Infrastructure
{
    public class MeetingService
    {
        private readonly MeetingRepository _meetingRepository;

        public MeetingService(MeetingRepository meetingRepository)
        {
            _meetingRepository = meetingRepository;
        }

        public IEnumerable<Meeting> GetCard(bool full)
        {
            return full ? _meetingRepository.GetFullCard().AsEnumerable() : _meetingRepository.GetCard().AsEnumerable();
        }
    }
}