﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Globalization;

namespace System
{
    /// <summary>
    /// extension methods helpers for common 
    /// </summary>
    public static class StringExtension
    {
        public static bool IsNullOrEmpty(this string text)
        {
            return string.IsNullOrEmpty(text);
        }

        public static string ToNullSafeString(this object value)
        {
            return value == null ? String.Empty : value.ToString();
        }

        public static string Capitalise(this string text)
        {
            return text == null ? null : CultureInfo.CurrentCulture.TextInfo.ToTitleCase(text);
        }

        public static bool ToBoolean(this string text)
        {
            return Convert.ToBoolean(text);
        }

        public static uint ToUInt32(this ValueType instance)
        {
            return Convert.ToUInt32(instance);
        }

        public static int ToNullSafeInt(this string value)
        {
            return String.IsNullOrEmpty(value) ? 0 : int.Parse(value);
        }

        public static int ToNullSafeWeightInt(this string value)
        {
            var cleanedWeight = value == null ? string.Empty : value.Replace("(", "").Replace(")", "");

            return ToNullSafeInt(cleanedWeight);
        }

        public static int ToNullSafeInt(this char value)
        {
            return value.ToString().ToNullSafeInt();
        }

        public static string ToHexString(this int value)
        {
            var sb = new StringBuilder();
            var input = value.ToString();

            foreach (char digit in input)
                sb.Append("{0:x2}".Fill(digit.ToUInt32()));

            return sb.ToString();
        }

        public static Boolean TryStringToGuid(String text, out Guid value)
        {
            try
            {
                value = new Guid(text);
                return true;
            }
            catch (FormatException)
            {
                value = Guid.Empty;
                return false;
            }
        }

        public static string TrimWithDotDotDot(this string str, int maxLength)
        {
            if (str.Length > maxLength)
                return str.Substring(0, maxLength) + "...";
            else
                return str;
        }

        public static string EnsureLength(this string str, int maxLength)
        {
            if ((str + string.Empty).Length > maxLength)
                return str.Substring(0, maxLength);
            else
                return str;
        }

        public static string RemoveRange(this string input, int startIndex, int endIndex)
        {
            return input.Remove(startIndex, endIndex - startIndex);
        }

        public static bool In<T>(this T source, params T[] list)
        {
            if (null == source) throw new ArgumentNullException("source");
            return list.Contains(source);
        }

        public static bool NotIn<T>(this T source, params T[] list)
        {
            if (null == source) throw new ArgumentNullException("source");
            return !list.Contains(source);
        }

        public static bool In<T>(this T source, IEnumerable<T> list)
        {
            if (null == source) throw new ArgumentNullException("source");
            return list.Contains(source);
        }

        public static bool NotIn<T>(this T source, IEnumerable<T> list)
        {
            if (null == source) throw new ArgumentNullException("source");
            return !list.Contains(source);
        }

        public static bool EqualsIgnoreCase(this string left, string right)
        {
            return String.Compare(left, right, true) == 0;
        }

        public static string FormatWith(this string format, params object[] args)
        {
            if (format == null)
                throw new ArgumentNullException("format");

            return string.Format(format, args);
        }

        public static string FormatWith(this string format, IFormatProvider provider, params object[] args)
        {
            if (format == null)
                throw new ArgumentNullException("format");

            return string.Format(provider, format, args);
        }

        public static int AppendReplacement(this Match match, StringBuilder sb, string input, string replacement, int index)
        {
            var preceding = input.Substring(index, match.Index - index);

            sb.Append(preceding);
            sb.Append(replacement);

            return match.Index + match.Length;
        }

        public static void AppendTail(this Match match, StringBuilder sb, string input, int index)
        {
            sb.Append(input.Substring(index));
        }

        public static string RegexReplace(this string input, string pattern, string replacement)
        {
            return Regex.Replace(input, pattern, replacement);
        }

        public static string RegexReplace(this string input, string pattern, string replacement, RegexOptions options)
        {
            return Regex.Replace(input, pattern, replacement, options);
        }

        public static string Fill(this string format, params object[] args)
        {
            return String.Format(format, args);
        }
        
        public static string ToCommaDelimitedString(this IEnumerable<string> list)
        {
            if (list.IsNullOrEmpty())
                return string.Empty;

            return String.Join(", ", list);
        }

        public static string ToCsvList(this string list)
        {
            return list.Replace(',', '|');
        }
        
        public static string GetFirstNCharacters(this string s, int n)
        {
            n = Math.Min(s.Length, n);
            return s.Substring(0, n);
        }

        public static string GetLastNCharacters(this string s, int n)
        {
            n = Math.Min(s.Length, n);
            return s.Substring(s.Length - n, n);
        }

        public static string StripLastNCharacters(this string s, int n)
        {
            n = Math.Min(s.Length, n);
            return s.Substring(0, s.Length - n);
        }
    }
}
