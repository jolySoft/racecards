﻿namespace System.Linq
{
    public static class LinqExtension
    {
        public static IQueryable<T> TakePage<T>(this IQueryable<T> query, int pageIndex, int pageSize)
        {
            return query.Skip(pageSize * pageIndex).Take(pageSize);
        }
    }
}
