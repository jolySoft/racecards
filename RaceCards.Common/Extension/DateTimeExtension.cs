﻿namespace System
{
    public static class DateTimeExtension
    {
        public static bool OnSameDayAs(this DateTime dateWithTime, DateTime searchDate)
        {
            return Convert.ToBoolean(dateWithTime >= searchDate.Date && dateWithTime <= searchDate.Date.AddDays(1).AddTicks(-1));
        }
    }
}
