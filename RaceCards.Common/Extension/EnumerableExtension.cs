﻿using System.Linq;

namespace System.Collections.Generic
{
    public static class EnumerableExtension
    {
        public static bool IsNotEmpty<T>(this IEnumerable<T> list)
        {
            return !list.IsNullOrEmpty();
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> list)
        {
            return (list == null || list.Count() == 0);
        }

        /// <summary>
        /// Count items in list - if null, returns 0
        /// </summary>
        public static int CountNullable<T>(this IEnumerable<T> list)
        {
            return list.IsNullOrEmpty() ? 0 : list.Count();
        }

        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var item in enumerable)
            {
                action(item);
            }
        }
    }
}
