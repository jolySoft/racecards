﻿using System;

namespace System
{
    public struct Fraction : IEquatable<Fraction>, IComparable, IComparable<Fraction>
    {
        private readonly int _originalNumerator;
        private readonly int _originalDenominator;
        private readonly int _numerator;
        private readonly int _denominator;
        
        public Fraction(int numerator, int denominator)
        {
            if (denominator == 0)
            {
                throw new ArgumentException("The denominator must not be equal to zero.", "denominator");
            }

            int sign = Math.Sign(numerator) * Math.Sign(denominator);
            _originalNumerator = sign * Math.Abs(numerator);
            _originalDenominator = Math.Abs(denominator);

            int gcd = GreatestCommonDivisor(numerator, denominator);
            _numerator = _originalNumerator / gcd;
            _denominator = _originalDenominator / gcd;
        }

        public Fraction(int numerator) : this(numerator, 1) { }

        public int Numerator
        {
            get { return _numerator; }
        }

        public int Denominator
        {
            get { return _denominator; }
        }

        public int OriginalNumerator
        {
            get { return _originalNumerator; }
        }

        public int OriginalDenominator
        {
            get { return _originalDenominator; }
        }

        public double Value
        {
            get { return (double)_numerator / _denominator; }
        }


        public bool Equals(Fraction other)
        {
            return other._numerator == _numerator &&
                   other._denominator == _denominator;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_numerator * 397) ^ _denominator;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}", _numerator, _denominator);
        }

        public override bool Equals(object obj)
        {
            return obj is Fraction && Equals((Fraction)obj);
        }

        public int CompareTo(Fraction other)
        {
            int lcm = LeastCommonMultiple(_denominator, other._denominator);
            int numerator = (lcm / _denominator) * _numerator;
            int otherNumerator = (lcm / other._denominator) * other._numerator;

            return numerator - otherNumerator;
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }

            if (obj is Fraction)
            {
                return CompareTo((Fraction)obj);
            }

            throw new ArgumentException("Object must be of type Fraction.");
        }


        public static bool operator ==(Fraction left, Fraction right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Fraction left, Fraction right)
        {
            return !left.Equals(right);
        }

        public static Fraction Parse(string fraction)
        {
            if (fraction == null)
            {
                throw new ArgumentNullException("fraction", "Input string cannot be null");
            }

            string[] parts = fraction.Split(new[] { '/' });
            if (parts.Length > 2)
            {
                throw new FormatException("Invalid Fraction format.");
            }

            var numerator = int.Parse(parts[0]);
            var denominator = parts.Length == 2 ? int.Parse(parts[1]) : 1;

            return new Fraction(numerator, denominator);
        }

        public static bool TryParse(string fraction, out Fraction result)
        {
            result = new Fraction();

            if (fraction == null)
            {
                return false;
            }

            string[] parts = fraction.Split(new[] { '/' });
            if (parts.Length > 2)
            {
                return false;
            }

            int numerator;
            if (!int.TryParse(parts[0], out numerator))
            {
                return false;
            }

            int denominator;
            if (!int.TryParse(parts.Length == 2 ? parts[1] : "1", out denominator))
            {
                return false;
            }

            if (denominator == 0)
            {
                return false;
            }

            result = new Fraction(numerator, denominator);
            return true;
        }

        public static int GreatestCommonDivisor(int a, int b)
        {
            if (a == 0 && b == 0)
            {
                throw new ArgumentException("The two numbers cannot be both zero.");
            }

            a = Math.Abs(a);
            b = Math.Abs(b);

            // Euclidean algorithm
            while (b != 0)
            {
                int temp = b;
                b = a % b;
                a = temp;
            }

            return a;
        }

        public static int LeastCommonMultiple(int a, int b)
        {
            return Math.Abs(a / GreatestCommonDivisor(a, b)) * Math.Abs(b);
        }
    }
}
