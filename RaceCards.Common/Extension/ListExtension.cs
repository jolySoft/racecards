﻿using System.Collections.Concurrent;
using System.Linq;

namespace System.Collections.Generic
{
    public static class ListExtension
    {
        public static SortedList<T, int> GetClusters<T>(this IEnumerable<T> list) where T : IComparable
        {
            return new SortedList<T, int>(
                list.GroupBy(li => li)
                    .ToDictionary(grouping => grouping.Key, grouping => grouping.Count()));
        }


        public static IEnumerable<IEnumerable<T>> Permutate<T>(this IEnumerable<T> list, int? count = null)
        {
            if (list == null) list = new T[0];
            if (!count.HasValue) count = list.Count();

            if (count == 0)
            {
                yield return new T[0];
            }
            else
            {
                int startingElementIndex = 0;
                foreach (T startingElement in list)
                {
                    var remainingItems = AllExcept(list, startingElementIndex);

                    foreach (var permutationOfRemainder in Permutate(remainingItems, count - 1))
                    {
                        yield return Concat(new[] { startingElement }, permutationOfRemainder);
                    }
                    startingElementIndex += 1;
                }
            }
        }

        private static IEnumerable<T> Concat<T>(IEnumerable<T> a, IEnumerable<T> b)
        {
            foreach (T item in a) { yield return item; }
            foreach (T item in b) { yield return item; }
        }

        private static IEnumerable<T> AllExcept<T>(IEnumerable<T> input, int indexToSkip)
        {
            int index = 0;
            foreach (T item in input)
            {
                if (index != indexToSkip) yield return item;
                index += 1;
            }
        }
    }
}
