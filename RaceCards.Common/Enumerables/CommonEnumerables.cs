﻿using System.ComponentModel;

namespace RaceCards.Common.Enumerables
{
    public class CommonEnumerables
    {
        [TypeConverter(typeof(EnumConverter<CategoryCode>))]
        public enum CategoryCode
        {
            [EnumCode(" ")] None,
            [EnumCode("HR")] Horse,
            [EnumCode("DG")] Dog
        }

        [TypeConverter(typeof(EnumConverter<YesOrNoStatus>))]
        public enum YesOrNoStatus
        {
            [EnumCode("Y")] Yes,
            [EnumCode("N")] No,
            [EnumCode(" ")] Unknown
        }

        [TypeConverter(typeof(EnumConverter<Country>))]
        public enum Country
        {
            [EnumCode("UK")] United_Kingdom,
            [EnumCode("IE")] Rep_of_Ireland,
            [EnumCode("AU")] Australia,
            [EnumCode("VR")] Virtual,
            [EnumCode("ZA")] South_Africa,
            [EnumCode("FR")] France,
            [EnumCode("US")] USA,
            [EnumCode("IR")] IR,
            [EnumCode("")] None
        }

        [TypeConverter(typeof(EnumConverter<GenderStatus>))]
        public enum GenderStatus
        {
            [EnumCode("M")] Male,
            [EnumCode("F")] Female,
            [EnumCode(" ")] Unknown
        }

        [TypeConverter(typeof(EnumConverter<SingleRaceBet>))]
        public enum SingleRaceBet
        {
            [EnumCode("N")] No_Tote_Wagering,
            [EnumCode("W")] Tote_Win_Pool,
            [EnumCode("P")] Tote_Place_Pool,
            [EnumCode("S")] Tote_Show_Pool,
            [EnumCode("X")] Tote_Exacta,
            [EnumCode("I")] Tote_Trifecta,
            [EnumCode("C")] Chart_Forecast,
            [EnumCode("F")] Forecast,
            [EnumCode("T")] Tricast,
            [EnumCode("J")] BAGS_Greyhound_Forecast,
            [EnumCode("K")] Non_BAGS_Greyhound_Forecast,
            [EnumCode("L")] BAGS_Greyhound_Tricast,
            [EnumCode("M")] Non_BAGS_Greyhound_Tricast,
            [EnumCode("E")] Computer_Straight_Forecast
        }

        [TypeConverter(typeof(EnumConverter<MultiRaceBet>))]
        public enum MultiRaceBet
        {
            [EnumCode("J")] Jackpot_Pool,
            [EnumCode("L")] Placepot_Pool,
            [EnumCode("W")] Place_56,
            [EnumCode("Q")] Quadpot
        }

        [TypeConverter(typeof(EnumConverter<DividendType>))]
        public enum DividendType // This is a realy bad way of doing it 
        {                        // but because of implementation restriction were stuck
            //Single
            [EnumCode("F")] Forecast,
            [EnumCode("T")] Tricast,
            //Multi
            [EnumCode("J")] Jackpot_Pool,
            [EnumCode("L")] Placepot_Pool,
            [EnumCode("W")] Place_56,
            [EnumCode("Q")] Quadpot
        }

        [TypeConverter(typeof(EnumConverter<MarketType>))]
        public enum MarketType
        {
            [EnumCode("A")] Antepost,
            [EnumCode("E")] Early_Prices,
            [EnumCode("S")] Starting_Prices,
            [EnumCode("T")] Tissue_Prices,
            [EnumCode("B")] Board_Prices
        }

        [TypeConverter(typeof(EnumConverter<BettingStatus>))]
        public enum BettingStatus
        {
            [EnumCode("X")] No_activity_yet,
            [EnumCode("0")] Opening_show_complete,
            [EnumCode("1")] Subsequent_show_complete_for_1st_market,
            [EnumCode("2")] Subsequent_show_complete_for_2nd_market,
            [EnumCode("3")] Subsequent_show_complete_for_3rd_market,
            [EnumCode("4")] Subsequent_show_complete_for_4th_market,
            [EnumCode("5")] Subsequent_show_complete_for_5th_market,
            [EnumCode("6")] Subsequent_show_complete_for_6th_market,
            [EnumCode("7")] Subsequent_show_complete_for_7th_market,
            [EnumCode("8")] Subsequent_show_complete_for_8th_market,
            [EnumCode("9")] SPs_complete
        }

        [TypeConverter(typeof(EnumConverter<DataProvider>))]
        public enum DataProvider
        {
            [EnumCode("SIS")]SIS,
            [EnumCode("FN")]Formnet,
            [EnumCode("PA")]Press_Association,
            [EnumCode("SDS")]SISFill,
            [EnumCode("EVE")]SISFill_Evening,
        }

        [TypeConverter(typeof(EnumConverter<EventStatus>))]
        public enum EventStatus
        {
            [EnumCode("O")] On,
            [EnumCode("E")] Replay,         //deprecated
            [EnumCode("D")] Delayed_Start,
            [EnumCode("P")] Postponed,      //deprecated
            [EnumCode("R")] Rescheduled,    //deprecated
            [EnumCode("C")] Cancelled,      //deprecated
            [EnumCode("A")] Abandoned,
            [EnumCode("V")] Void,
            [EnumCode("S")] Standby         //deprecated
        }

        [TypeConverter(typeof(EnumConverter<EventProgress>))]
        public enum EventProgress
        {
            [EnumCode("D")] Start_Delay,
            [EnumCode("O")] Off,
            [EnumCode("M")] Betng_Suspnd,
            [EnumCode("N")] No_Race,
            [EnumCode("X")] Free_Format  
        }

        [TypeConverter(typeof(EnumConverter<MeetingStatus>))]
        public enum MeetingStatus
        {
            [EnumCode("O")] On,
            [EnumCode("E")] Replay,         //deprecated
            [EnumCode("D")] Delayed_Start,
            [EnumCode("P")] Postponed,      //deprecated
            [EnumCode("R")] Rescheduled,    //deprecated
            [EnumCode("C")] Cancelled,      //deprecated
            [EnumCode("A")] Abandoned,
            [EnumCode("V")] Void,
            [EnumCode("S")] Standby,        //deprecated
            [EnumCode("X")] Deleted
        }

        [TypeConverter(typeof(EnumConverter<FavouriteStatus>))]
        public enum FavouriteStatus
        {
            [EnumCode("Fav")]  Favourite,
            [EnumCode("jf")]   Joint_favourite,
            [EnumCode("cf3")]  co__fav_of_three,
            [EnumCode("cf4")]  co__fav_of_four,
            [EnumCode("cf5")]  co__fav_of_five,
            [EnumCode("cf6")]  co__fav_of_six,
            [EnumCode("cf7")]  co__fav_of_seven,
            [EnumCode("cf8")]  co__fav_of_eight,
            [EnumCode("2f")]   Second_favourite,
            [EnumCode("j2f")]  Joint_second_favourite,
            [EnumCode("c2f3")] co__second_favourite_of_three,
            [EnumCode("c2f4")] co__second_favourite_of_four,
            [EnumCode("c2f5")] co__second_favourite_of_five,
            [EnumCode("c2f6")] co__second_favourite_of_six,
            [EnumCode("c2f7")] co__second_favourite_of_seven,
            [EnumCode("")] none
        }

        [TypeConverter(typeof(EnumConverter<RunnerStatus>))]
        public enum RunnerStatus
        {
            [EnumCode("G")] Goes_as_expected,
            [EnumCode("N")] Non_Runner,
            [EnumCode("W")] Withdrawn,
            [EnumCode("R")] Reserve,
            [EnumCode("V")] Vacant
        }

        [TypeConverter(typeof(EnumConverter<ResultStatus>))]
        public enum ResultStatus
        {
            [EnumCode(" ")] Unknown,
            [EnumCode("O")] Off,
            [EnumCode("Q")] May_Be_Enquiry,
            [EnumCode("R")] Objection,
            [EnumCode("S")] Stewards_Enquiry,
            [EnumCode("V")] Result_Stands,
            [EnumCode("W")] Weighed_In,
            [EnumCode("Y")] Result_Complete,
            [EnumCode("N")] No_Race,
            [EnumCode("D")] Void,
            [EnumCode("Z")] Free_Format,
        }
    }
}
