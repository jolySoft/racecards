﻿using NHibernate;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

namespace RaceCards.Common.Enumerables
{
    public abstract class ValueConverter<T> : IUserType where T : struct
    {
        protected System.ComponentModel.TypeConverter Converter = System.ComponentModel.TypeDescriptor.GetConverter(typeof(T));

        public object Assemble(object cached, object owner)
        {
            return DeepCopy(cached);
        }

        public object DeepCopy(object value)
        {
            return value;
        }

        public object Disassemble(object value)
        {
            return DeepCopy(value);
        }

        public new bool Equals(object x, object y)
        {
            if (x == null || y == null)
                return x == null && y == null;
            return x.Equals(y);
        }

        public int GetHashCode(object x)
        {
            return x.GetHashCode();
        }

        public bool IsMutable
        {
            get { return false; }
        }

        public object NullSafeGet(System.Data.IDataReader rs, string[] names, object owner)
        {
            object obj = NHibernateUtil.String.NullSafeGet(rs, names);
            return (obj == null) ? null : Converter.ConvertFromString((string)obj);
        }

        public void NullSafeSet(System.Data.IDbCommand cmd, object value, int index)
        {
            if (value != null)
            {
                string NativeValue = Converter.ConvertToString(value);
                NHibernateUtil.String.NullSafeSet(cmd, NativeValue, index);
            }
        }

        public object Replace(object original, object target, object owner)
        {
            return original;
        }

        public System.Type ReturnedType
        {
            get { return typeof(T); }
        }

        public abstract SqlType[] SqlTypes
        {
            get;
        }
    }
}
