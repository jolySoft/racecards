﻿using NHibernate.SqlTypes;

namespace RaceCards.Common.Enumerables
{

    public class YesNoStatusEnum : ValueConverter<CommonEnumerables.YesOrNoStatus>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }

    public class CountryEnum : ValueConverter<CommonEnumerables.Country>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(2);
            }
        }
    }

    public class GenderEnum : ValueConverter<CommonEnumerables.GenderStatus>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }

    public class MeetingStatusEnum : ValueConverter<CommonEnumerables.MeetingStatus>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }

    public class DogMeetingAuthorityEnum : ValueConverter<DogEnumerables.Authority>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(2);
            }
        }
    }

    public class DogMeetingServiceTypeEnum : ValueConverter<DogEnumerables.ServiceType>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(10);
            }
        }
    }

    public class EventStatusEnum : ValueConverter<CommonEnumerables.EventStatus>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }

    public class DogEventBatchMarketTypeEnum : ValueConverter<CommonEnumerables.MarketType>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get { return GetSqlType.Length(1); }
        }
    }

    public class DividendTypeEnum : ValueConverter<CommonEnumerables.DividendType>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(2);
            }
        }
    }

    public class MarketTypeEnum : ValueConverter<CommonEnumerables.MarketType>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }

    public class DogSurfaceTypeEnum : ValueConverter<DogEnumerables.SurfaceType>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }

    public class DogEventProgressEnum : ValueConverter<DogEnumerables.EventProgress>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }

    public class EventBettingStatusEnum : ValueConverter<CommonEnumerables.BettingStatus>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }
  
    public class DogStatusEnum : ValueConverter<DogEnumerables.DogStatus>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }


    public class MultiRaceBetTypeEnum : ValueConverter<CommonEnumerables.MultiRaceBet>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }

    public class SingleRaceBetTypeEnum : ValueConverter<CommonEnumerables.SingleRaceBet>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }

    public class DogResultStatusEnum : ValueConverter<CommonEnumerables.ResultStatus>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }

    public class DogResultSettlingStatusEnum : ValueConverter<DogEnumerables.SettlingStatus>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }

    public class DogResultServiceTypeEnum : ValueConverter<DogEnumerables.ServiceType>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(12);
            }
        }
    }

    public class DogEventServiceEnum : ValueConverter<DogEnumerables.Service>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(4);
            }
        }
    }


    public class DogResultDataProviderEnum : ValueConverter<CommonEnumerables.DataProvider>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(3);
            }
        }
    }

    public class DogCourseTypeEnum : ValueConverter<DogEnumerables.CourseType>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(1);
            }
        }
    }

    public class CategoryCodeEnum : ValueConverter<CommonEnumerables.CategoryCode>, ITypeEnum
    {
        public override SqlType[] SqlTypes
        {
            get
            {
                return GetSqlType.Length(2);
            }
        }
    }

}
