﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NHibernate.SqlTypes;

namespace RaceCards.Common.Enumerables
{
    public static class EnumHelper
    {
        private struct EnumCodeCacheItem
        {
            public object Enum { get; set; }
            public string EnumCode { get; set; }
        }

        private static Dictionary<Type, EnumCodeCacheItem[]> _enumCodeCache = null;

        public static IEnumerable<Enum> Enumerate<T>(this T enumeration) where T : struct
        {
            return Enum.GetValues(typeof(T)).Cast<Enum>();
        }

        /// <summary>
        /// extension for enums to get display-friendly version without the underscores
        /// </summary>
        public static string Description(this Enum enumValue)
        {
            return Description(enumValue.ToNullSafeString());
        }

        public static string ToDescription(Enum enumeration)
        {
            return Description(enumeration.ToString());
        }

        public static string Description(string x)
        {
            string res = x.Replace("__", "-").Replace("_", " ");
            return res;
        }

        /// <summary>
        /// convert string into a given enum type value
        /// </summary>
        public static T ParseCode<T>(string code) where T : struct
        {
            if (code == "_") { code = " "; }
            var value = new EnumConverter<T>().ConvertFrom(code);
            return (T)(value);
        }

        public static string ToCode(Enum enumeration)
        {
            return enumeration.Code();
        }

        public static string Code(this Enum enumeration)
        {
            var result = (from i in GetEnumCodeCache(enumeration.GetType())
                          where i.Enum.Equals(enumeration)
                          select i.EnumCode == " " ? "_" : i.EnumCode)
                         .SingleOrDefault();

            return result ?? enumeration.ToString();
        }

        public static T GetByEnumCode<T>(string strValue)
        {
            if (strValue == null)
                return default(T);

            if (strValue == "_")
                strValue = " ";
            else if (strValue != " ")
                strValue = strValue.Trim();

            var result = GetEnumCodeCache(typeof (T)).Where(i => i.EnumCode == strValue).Select(i => i.Enum).SingleOrDefault();

            if (result == null)
                throw new ArgumentException(string.Format("No matching Enum to, {0} were found for value, '{1}'",typeof(T).FullName,  strValue), "strValue");

            return (T)result;
        }

        public static string GetEnumCodeText<T>(object enumItem)
        {
            if (enumItem == null)
                throw new ArgumentNullException("enumItem");

            var ft = Enum.ToObject(typeof(T), enumItem);
            return GetEnumCodeCache(typeof (T)).Single(i => i.Enum.Equals(ft)).EnumCode;
        }

        private static IEnumerable<EnumCodeCacheItem> GetEnumCodeCache(Type type)
        {
            if (_enumCodeCache == null)
                _enumCodeCache = new Dictionary<Type, EnumCodeCacheItem[]>();

            if (!_enumCodeCache.ContainsKey(type))
            {
                _enumCodeCache.Add(
                    type,
                    (from a in type.GetFields(BindingFlags.Static | BindingFlags.Public)
                     from b in (EnumCode[])a.GetCustomAttributes(typeof(EnumCode), false)
                     select new EnumCodeCacheItem
                     {
                         EnumCode = b.Text,
                         Enum = Enum.Parse(type, a.Name)
                     })
                    .ToArray()
                );
            }

            return _enumCodeCache[type];
        }
    }

    public class EnumCode : Attribute
    {
        public string Text { get; private set; }
        public EnumCode(string text) { Text = text; }
    }

    public interface ITypeEnum
    {
        SqlType[] SqlTypes { get; }
    }

    public static class GetSqlType
    {
        public static SqlType[] Length(int length)
        {
            SqlType[] sqlTypes = { SqlTypeFactory.GetString(length) };
            return sqlTypes;
        }
    }
}
