﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace RaceCards.Common.Enumerables
{
    public class EnumConverter<T> : EnumConverter where T : struct
    {
        public EnumConverter() : base(typeof(T)) { }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (object.ReferenceEquals(sourceType, typeof(string)) || object.ReferenceEquals(sourceType, typeof(char)));
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (!(value is string))
                throw new NotSupportedException();
            return EnumHelper.GetByEnumCode<T>(value.ToString());
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (object.ReferenceEquals(destinationType, typeof(string)))
                return true;

            throw new NotSupportedException();
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            return EnumHelper.GetEnumCodeText<T>(value);
        }
    }
}
