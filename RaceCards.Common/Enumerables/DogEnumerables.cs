﻿using System;
using System.ComponentModel;

namespace RaceCards.Common.Enumerables
{
    /// <summary>
    /// Contains all the DOG Enumerables for various status'
    /// </summary>
    public class DogEnumerables //: IEnumerablesContainer
    {
        [TypeConverter(typeof(EnumConverter<Service>))]
        public enum Service{
            [EnumCode("MAIN")] Main,
            [EnumCode("EMP")] Early_Morning_Product,
            [EnumCode("EVE")] Evening_Dogs,
        }

        [TypeConverter(typeof(EnumConverter<SurfaceType>))]
        public enum SurfaceType
        {
            //note: looked at DB for Dog specific surface types, only brought back Dirt and Unknown.
            [EnumCode("A")] All_Weather,
            //            [EnumCode("C")] Clay,
            [EnumCode("D")] Dirt,
            //            [EnumCode("I")] Indoor,
            //            [EnumCode("L")] Lawn,
            //            [EnumCode("N")] Sand,
            [EnumCode("O")] Outdoor,
            [EnumCode("T")] Turf,
            //            [EnumCode("V")] Various,
            [EnumCode(" ")] Unknown
        }

        [TypeConverter(typeof(EnumConverter<Authority>)), Flags]
        public enum Authority
        {
            [EnumCode("NA")] None,
            [EnumCode("BA")] BAGS, // Dogs
            [EnumCode("NB")] Non__BAGS, // Dogs
            [EnumCode("VR")] Virtual,
            [EnumCode("FL")] Flat, // Horses
            [EnumCode("NH")] National_Hunt, // Horses
            [EnumCode("TR")] Trotting, // Horses
            [EnumCode("")] Unknown
        }

        [TypeConverter(typeof(EnumConverter<ServiceType>))]
        public enum ServiceType
        {
            [EnumCode("FULL")] Full,
            [EnumCode("RESULT_ONLY")] Results_Only
        }

        [TypeConverter(typeof(EnumConverter<CourseType>))]
        public enum CourseType
        {
            //[EnumCode("C")] Chase,
            [EnumCode(" ")] Unknown,
            [EnumCode("F")] Flat,
            [EnumCode("H")] Hurdle
            
        }

        [TypeConverter(typeof(EnumConverter<EventProgress>))]
        public enum EventProgress
        {
            [EnumCode(" ")] Unknown,
            [EnumCode("A")] Parading,
            [EnumCode("B")] B,
            [EnumCode("C")] App_Traps,
            [EnumCode("E")] Going_In_Tps,
            [EnumCode("H")] Hare_Running,
            [EnumCode("O")] Off,
            [EnumCode("D")] Start_Delay,
            [EnumCode("J")] Hare_Stopped,
            [EnumCode("K")] Traps_Failed,
            [EnumCode("M")] Betng_Suspnd,
            [EnumCode("N")] No_Race,
            [EnumCode("X")] Free_Format,
            [EnumCode("R")] ReRun   //X+RERUN
        }

        public enum PriceType
        {
            [EnumCode("Show")] Show,
            [EnumCode("SP")] SP         
        }


        [TypeConverter(typeof(EnumConverter<DogStatus>))]
        public enum DogStatus
        {
            [EnumCode("G")] Goes_as_expected,
            [EnumCode("V")] Vacant,
            [EnumCode("R")] Reserve,           
        }

        [TypeConverter(typeof(EnumConverter<SettlingStatus>))]
        public enum SettlingStatus
        {
            [EnumCode("")] No_results_available_yet,
            [EnumCode("F")] Frame_Result_less_places_received_than_expected,
            [EnumCode("P")] Result_Places_Complete,
            [EnumCode("S")] Result_Places_and_win_eachway_places_complete,
            [EnumCode("D")] Result_Places_Winning_SPs_and_dividends_complete,
            [EnumCode("H")] Hold_Result_Objection_Stewards_etc,
            [EnumCode("R")] Result_Fully_Complete,
            [EnumCode("C")] Result_Correction,
            [EnumCode("V")] Result_void
        }
    }  
}
